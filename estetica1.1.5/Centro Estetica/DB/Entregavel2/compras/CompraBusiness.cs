﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Centro_Estetica.DB.Base.Entregavel2.compras
{
    public class CompraBusiness
    {
        CompraDatabase db = new CompraDatabase();

        public int Salvar(CompraDTO compra)
        {
            return db.Salvar(compra);
        }

        public List<CompraDTO> Consultar(string produto)
        {
            return db.Consultar(produto);
        }

        public List<CompraDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
