﻿using Centro_Estetica.DB.Base.Entregavel1.controle_Funcionario;
using Centro_Estetica.TELAS.Esqueci_a_Senha;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Centro_Estetica.TELAS
{
    public partial class frmLogim : Form
    {
        public frmLogim()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;
                
                    frmMenu tela = new frmMenu();
                    tela.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "High Estetic", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblDados_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Esqueci tela = new Esqueci();
            tela.Show();
            Hide();
        }
    }
    }

