﻿using Centro_Estetica.DB.Base.Entregavel2.compras;
using Centro_Estetica.DB.Base.Entregavel2.Foncesedor;
using System;
using System.Windows.Forms;

namespace Centro_Estetica.TELAS.Compras
{
    public partial class frmCadastarCompras : Form
    {
        public frmCadastarCompras()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cboFornecedor.SelectedItem as FornecedorDTO;
                ComprasDTO dto = new ComprasDTO();
                dto.IdFornec = fornecedor.Id;
                dto.Nome = txtNome.Text;
                dto.Quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.Total = Convert.ToDecimal(txtTotal.Text);
                dto.Data = DateTime.Now;

                ComprasBusiness bussiness = new ComprasBusiness();
                bussiness.Salvar(dto);

                MessageBox.Show("Compra efetuada com sucesso.", "High Estetic",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
