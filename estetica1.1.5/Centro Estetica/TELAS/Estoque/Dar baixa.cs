﻿using Centro_Estetica.DB.Base.Entregavel2.compras;
using Centro_Estetica.TELAS.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Centro_Estetica.TELAS.Estoque
{
    public partial class Dar_baixa : Form
    {
        public Dar_baixa()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {

            ComprasBusiness buss = new ComprasBusiness();
            List<ComprasDTO> listas = buss.Listar();
            cboProduto.ValueMember = nameof(ComprasDTO.Id);
            cboProduto.DisplayMember = nameof(ComprasDTO.Nome);
            cboProduto.DataSource = listas;
        }

        private void btnBaixa_Click(object sender, EventArgs e)
        {
            {
                ComprasDTO cat = cboProduto.SelectedItem as ComprasDTO;

                ComprasDTO dto = new ComprasDTO();

                dto.Id = cat.Id;
                dto.Quantidade = Convert.ToInt32(txtQuantidade.Text.Trim());

                ComprasBusiness a = new ComprasBusiness();
                a.DarBaixa(dto);

                MessageBox.Show("Estoque alterado com sucesso.", "Dales Sorrisos",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
            }
        }

        private void btnSair_Click_1(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }
    }
}
