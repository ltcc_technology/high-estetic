﻿using Centro_Estetica.DB.Base.Entregavel2.Produto;
using Centro_Estetica.DB.Base.Entregavel3.Cliente;
using Centro_Estetica.DB.Base.Entregavel3.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Centro_Estetica.TELAS.Pedido
{
    public partial class frmCadastrarPedido : Form
    {
        BindingList<ProdutoDTO> produtos = new BindingList<ProdutoDTO>();

        decimal valordavenda = 0;


        public frmCadastrarPedido()
        {
            InitializeComponent();
        }
        void CarregarCombos()
        {
            PacientesBusiness sla = new PacientesBusiness();
            List<PacientesDTO> list = sla.Listar();
            cboClientes.ValueMember = nameof(PacientesDTO.Id);
            cboClientes.DisplayMember = nameof(PacientesDTO.Nome);
            cboClientes.DataSource = list;

            produtoBusiness busin = new produtoBusiness();
            List<ProdutoDTO> lista = busin.Listar();
            cboTratamento.ValueMember = nameof(ProdutoDTO.Id);
            cboTratamento.DisplayMember = nameof(ProdutoDTO.Nome);
            cboTratamento.DataSource = list;

            produtoBusiness bus = new produtoBusiness();
            List<ProdutoDTO> li = busin.Listar();
            cboValor.ValueMember = nameof(ProdutoDTO.Id);
            cboValor.DisplayMember = nameof(ProdutoDTO.Valor);
            cboValor.DataSource = list;

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void btnSalvar_Click_2(object sender, EventArgs e)
        {
            try
            {
                PacientesDTO cliente = cboClientes.SelectedItem as PacientesDTO;

                PedidoDTO dto = new PedidoDTO();

                dto.IdPaciente = cliente.Id;
                dto.Paciente = cboClientes.Text;
                dto.Tratamento = cboTratamento.Text;
                dto.Data = DateTime.Now;
                dto.Total = Convert.ToDecimal(cboValor.Text);
                dto.FormaPagamento = cboFormadePag.Text;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtos.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "High Estetic",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "High Estetic",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}




