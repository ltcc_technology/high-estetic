﻿using Centro_Estetica.TELAS.Cliente;
using Centro_Estetica.TELAS.Estoque;
using Centro_Estetica.TELAS.FluxoDcaixa;
using Centro_Estetica.TELAS.folha_de_pagamento;
using Centro_Estetica.TELAS.Fonecedor;
using Centro_Estetica.TELAS.Funcionario;
using Centro_Estetica.TELAS.Pedido;
using Centro_Estetica.TELAS.Produto;
using System;
using System.Windows.Forms;

namespace Centro_Estetica.TELAS
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }


        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Close();
        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFolhaPagamento tela = new frmFolhaPagamento();
            tela.Show();
            Hide();
        }

        private void funciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario tela = new frmCadastrarFuncionario();
            tela.Show();
            Hide();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmClienteCadastrar tela = new frmClienteCadastrar();
            tela.Show();
            Hide();
        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor tela = new frmCadastrarFornecedor();
            tela.Show();
            Hide();
        }

        private void serviçosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarTramamento tela = new frmCadastrarTramamento();
            tela.Show();
            Hide();
        }

        private void funcionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario tela = new frmConsultarFuncionario();
            tela.Show();
            Hide();
        }

        private void serviçosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarTratamento tela = new frmConsultarTratamento();
            tela.Show();
            Hide();
        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmClienteConsultar tela = new frmClienteConsultar();
            tela.Show();
            Hide();
        }

        private void fornecedoresToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor tela = new frmConsultarFornecedor();
            tela.Show();
            Hide();
        }

       
        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvconsultacompra tela = new dgvconsultacompra();
            tela.Show();
            Hide();
        }



        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarCaixa tela = new frmConsultarCaixa();
            tela.Show();
            Hide();
        }

        private void estoqueToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            dgvconsultacompra tela = new dgvconsultacompra();
            tela.Show();
            Hide();
        }

        private void pedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido tela = new frmCadastrarPedido();
            tela.Show();
            Hide();

        }

        private void darBaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dar_baixa tela = new Dar_baixa();
            tela.Show();
            Hide();
        }

        private void serviçosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar tela = new frmPedidoConsultar();
            tela.Show();
            Hide();
        }
    }
}
